function divide() {
    let userInput = prompt("Enter a number");

    if (userInput < 50) {
        console.log("Need to Terminate the loop");
    } else {
        if (Number.isInteger(userInput / 10)) {
            console.log(`${userInput} is divisible by 10`);
        } else if (Number.isInteger(userInput / 5)) {
            console.log(`${userInput}`);
        } else {
            divide();
        }
    }
}

divide();
